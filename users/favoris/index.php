<?php
include('../../simple_html_dom.php');

header('Content-type: application/json; charset=UTF-8');

if(isset($_GET['userId'])){

    $userId = $_GET['userId'];

    $urlUserPage = 'https://danstonchat.com/geek/' . $userId . '.html';
    $contentUserPage = getContent($urlUserPage);

    $favorisPage = getFavorisPage($contentUserPage);

    $favorisId = getFavorisId($userId,$favorisPage);
    echo json_encode( array( 'src' => $favorisId));

}

function getContent($urlUserPage){
        $cl = file_get_contents($urlUserPage);
        $html = new simple_html_dom();
        $html->load($cl);
        return $html;
}

function getFavorisPage($content){
    $div = $content->find('div[class=gravatar]')[0]->next_sibling()->next_sibling()->children(2);
    $favoriPageRaw = $div->plaintext;
    $favoriPageRaw = str_replace('Nombre de favoris : ','',$favoriPageRaw);
    $favoriPageRaw = str_replace(' (Voir ses favoris)','',$favoriPageRaw);
    $favorisPage = ($favoriPageRaw-$favoriPageRaw%25)/25 + 1;
    return $favorisPage;
}

function getFavorisId($userId,$favorisPage){
    $favorisId = array();
    for($i=0;$i<$favorisPage;$i++){
        $url = 'https://danstonchat.com/bookmarks/' . $userId . '/' . $i . '.html';

        $content = getContent($url);

        $divs = $content->find('div[class=item]');
        foreach ($divs as $div) {
            $idRaw = $div->class;
            $id = substr($idRaw,9);
            $favorisId[] = $id;
        }
    }

    return $favorisId;
}
