<?php
include('../simple_html_dom.php');

header('Content-type: application/json; charset=ISO-8859-1');

if(isset($_GET['quoteId'])){
    $quoteId = $_GET['quoteId'];

    $content = getContent($quoteId);

    $plusCount = getPlus($content, $quoteId) . "";
    $plusCount = substr($plusCount,4);
    $minusCount = getMinus($content, $quoteId) . "";
    $minusCount = substr($minusCount,4);
    $commentsId = getCommentsId($content, $quoteId);
    $quote = getQuote($content, $quoteId);

    echo json_encode(array(
            'quote' => $quote,
            'commentsId' => $commentsId ,
            'plus' => $plusCount ,
            'minus' => $minusCount
        ), JSON_PRETTY_PRINT);
}

function getContent($quoteId){
        $cl = file_get_contents('https://danstonchat.com/' . $quoteId . '.html');
        $html = new simple_html_dom();
        $html->load($cl);
        return $html;
}

function getQuote($content, $quoteId){
    $comments = $content->find('p[class=item-content]')[0]->children(0);
    $commentsWithoutBr = preg_replace("/\<br (.*?)\>/","",$comments->innertext);
    $commentsId = preg_replace("/\<span class=\"decoration\"\>/","</span>",$commentsWithoutBr);
    $commentsIdSplit = preg_split("/\<\/span\>/",$commentsId);
    $answer = array();
    for($i=1;$i<count($commentsIdSplit);$i+=2){
        $answer[] = array('user' => $commentsIdSplit[$i],
                        'sentence' => $commentsIdSplit[$i+1]);
    }
    return $answer;
}

function getCommentsId($content, $quoteId){
    $comments = $content->find('div[id=comments]')[0]->children();
    $commentsId = array();
    foreach($comments as $div){
        $id = $div->id . "";
        $id = substr($id,1);
        $commentsId[] = htmlentities($id);
    }
    return $commentsId;
}

function getMinus($content, $quoteId){
    $div = $content->find('a[href=https://danstonchat.com/voteminus/' . $quoteId . '.html]');
    return $div[0]->plaintext;
}

function getPlus($content, $quoteId){
    $div = $content->find('a[href=https://danstonchat.com/voteplus/' . $quoteId . '.html]');
    return $div[0]->plaintext;
}
