<?php
include('../../simple_html_dom.php');

header('Content-type: application/json; charset=UTF-8');

if(isset($_GET['quoteId'])&& isset($_GET['commentId'])){

    $quoteId = $_GET['quoteId'];
    $commentId = $_GET['commentId'];

    $content = getContent($quoteId);

    $plusCount = getPlus($content, $commentId) . "";
    $plusCount = substr($plusCount,4);
    $minusCount = getMinus($content, $commentId) . "";
    $minusCount = substr($minusCount,4);
    $comment = getComment($content, $commentId) . "";
    $authorId = getAuthorId($content, $commentId) . "";
    $authorId = str_replace('https://danstonchat.com/geek/', '' , $authorId );
    $authorId = str_replace('.html', '' , $authorId );

    echo json_encode(array(
            'authorId' => $authorId ,
            'comment' => htmlentities($comment) ,
            'plus' => $plusCount ,
            'minus' => $minusCount
        ));
}else{
    echo json_encode(array(
            'commentId' => '' ,
            'authorId' => '' ,
            'comment' => '',
            'plus' => '' ,
            'minus' => ''
        ));
}


function getContent($quoteId){
        $cl = file_get_contents('https://danstonchat.com/' . $quoteId . '.html');
        $html = new simple_html_dom();
        $html->load($cl);
        return $html;
}

function getAuthorId($content, $commentId){
    $div = $content->find('div[id=c' . $commentId . ']')[0]->children(1)->children(0);
    return $div->href;
}

function getComment($content, $commentId){
    $div = $content->find('div[id=c' . $commentId . ']')[0]->children(1)->children(1);
    return $div->plaintext;
}

function getMinus($content, $commentId){
    $div = $content->find('a[href=https://danstonchat.com/comment-voteminus/' . $commentId . '.html]');
    return $div[0]->plaintext;
}

function getPlus($content, $commentId){
    $div = $content->find('a[href=https://danstonchat.com/comment-voteplus/' . $commentId . '.html]');
    return $div[0]->plaintext;
}
