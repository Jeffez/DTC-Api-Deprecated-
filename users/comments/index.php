<?php
include('../../simple_html_dom.php');

header('Content-type: application/json; charset=UTF-8');

if(isset($_GET['userId'])){

    $userId = $_GET['userId'];

    $urlUserPage = 'https://danstonchat.com/geek/' . $userId . '.html';
    $contentUserPage = getContent($urlUserPage);

    $commentsPage = getCommentsPage($contentUserPage);

    $commentsId = getCommentsId($userId,$commentsPage);
    echo json_encode( array( 'src' =>  $commentsId));

}

function getContent($urlUserPage){
        $cl = file_get_contents($urlUserPage);
        $html = new simple_html_dom();
        $html->load($cl);
        return $html;
}

function getCommentsPage($content){
    $div = $content->find('div[class=gravatar]')[0]->next_sibling()->next_sibling()->children(1);
    $commentPageRaw = $div->plaintext;
    $commentPageRaw = str_replace('Nombre de commentaires : ','',$commentPageRaw);
    $commentPageRaw = str_replace(' (Voir ses commentaires)','',$commentPageRaw);
    $commentsPage = ($commentPageRaw-$commentPageRaw%25)/25 + 1;
    return $commentsPage;
}

function getCommentsId($userId,$commentsPage){
    $commentsId = array();
    for($i=0;$i<$commentsPage;$i++){
        $url = 'https://danstonchat.com/comments/' . $userId . '/' . $i . '.html';

        $content = getContent($url);

        $divs = $content->find('div[class=item]');
        foreach ($divs as $div) {
            $idRaw = $div->children(0)->id;
            $id = substr($idRaw,1);
            $quoteId = $div->children(2)->children(0)->id;
            $commentsId[] = array( 'quote' => $quoteId.
                                    'comment' => $id;
        }
    }

    return $commentsId;
}
